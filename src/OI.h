// RobotBuilder Version: 2.0
//
// This file was generated by RobotBuilder. It contains sections of
// code that are automatically generated and assigned by robotbuilder.
// These sections will be updated in the future when you export to
// C++ from RobotBuilder. Do not put any code or make any change in
// the blocks indicating autogenerated code or it will be lost on an
// update. Deleting the comments indicating the section will prevent
// it from being updated in the future.


#ifndef OI_H
#define OI_H

#include "WPILib.h"

#define XBOX_A_BUTTON 1
#define XBOX_B_BUTTON 2
#define XBOX_X_BUTTON 3
#define XBOX_Y_BUTTON 4
#define XBOX_LEFT_SHOLDER_BUTTON 5
#define XBOX_RIGHT_SHOLDER_BUTTON 6
#define XBOX_BACK_BUTTON 7
#define XBOX_START_BUTTON 8
#define XBOX_LEFT_STICK_BUTTON 9
#define XBOX_RIGHT_STICK_BUTTON 10

#define XBOX_LEFT_STICK_X_AXIS 0
#define XBOX_LEFT_STICK_Y_AXIS 1
#define XBOX_LEFT_TRIGGER_AXIS 2
#define XBOX_RIGHT_TRIGGER_AXIS 3
#define XBOX_RIGHT_STICK_X_AXIS 4
#define XBOX_RIGHT_STICK_Y_AXIS 5


#define EAT_BALL_BUTTON XBOX_RIGHT_SHOLDER_BUTTON
#define SPIT_BALL_BUTTON XBOX_RIGHT_SHOLDER_BUTTON

#define ARM_RAISE_BUTTON XBOX_X_BUTTON
#define ARM_LOWER_BUTTON XBOX_B_BUTTON
#define ARM_EXTEND_BUTTON XBOX_Y_BUTTON
#define ARM_WINCH_BUTTON XBOX_A_BUTTON
#define ARM_WINCH_FULL_POWER_BUTTON XBOX_LEFT_SHOLDER_BUTTON


class OI {
private:

	std::shared_ptr<Joystick> xBoxController;
	std::shared_ptr<Joystick> xBoxController2;

public:
	OI();

	std::shared_ptr<Joystick> getXBoxController();
	std::shared_ptr<Joystick> getXBoxController2();

};

#endif
